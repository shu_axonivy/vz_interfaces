[Ivy]
[>Created: Fri Aug 25 10:18:11 CEST 2017]
15E145CDF8F8F9CC 3.18 #module
>Proto >Proto Collection #zClass
Ds0 Dyamex_Test_Process Big #zClass
Ds0 B #cInfo
Ds0 #process
Ds0 @TextInP .resExport .resExport #zField
Ds0 @TextInP .type .type #zField
Ds0 @TextInP .processKind .processKind #zField
Ds0 @AnnotationInP-0n ai ai #zField
Ds0 @MessageFlowInP-0n messageIn messageIn #zField
Ds0 @MessageFlowOutP-0n messageOut messageOut #zField
Ds0 @TextInP .xml .xml #zField
Ds0 @TextInP .responsibility .responsibility #zField
Ds0 @StartRequest f0 '' #zField
Ds0 @EndTask f1 '' #zField
Ds0 @GridStep f3 '' #zField
Ds0 @GridStep f4 '' #zField
Ds0 @GridStep f5 '' #zField
Ds0 @RichDialog f6 '' #zField
Ds0 @PushWFArc f7 '' #zField
Ds0 @PushWFArc f2 '' #zField
Ds0 @PushWFArc f8 '' #zField
Ds0 @PushWFArc f9 '' #zField
Ds0 @PushWFArc f10 '' #zField
>Proto Ds0 Ds0 Dyamex_Test_Process #zField
Ds0 f0 outLink start.ivp #txt
Ds0 f0 type vzbank.in.test.Dyamex_Test_ProcessData #txt
Ds0 f0 inParamDecl '<> param;' #txt
Ds0 f0 actionDecl 'vzbank.in.test.Dyamex_Test_ProcessData out;
' #txt
Ds0 f0 guid 15E145CDFC1B390C #txt
Ds0 f0 requestEnabled true #txt
Ds0 f0 triggerEnabled false #txt
Ds0 f0 callSignature start() #txt
Ds0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ds0 f0 @C|.responsibility Everybody #txt
Ds0 f0 81 49 30 30 -21 17 #rect
Ds0 f0 @|StartRequestIcon #fIcon
Ds0 f1 type vzbank.in.test.Dyamex_Test_ProcessData #txt
Ds0 f1 81 465 30 30 0 15 #rect
Ds0 f1 @|EndIcon #fIcon
Ds0 f3 actionDecl 'vzbank.in.test.Dyamex_Test_ProcessData out;
' #txt
Ds0 f3 actionTable 'out=in;
' #txt
Ds0 f3 actionCode 'import vzbank_integration_test.DyamexTest;

in.client  = DyamexTest.initDyamexClient();' #txt
Ds0 f3 type vzbank.in.test.Dyamex_Test_ProcessData #txt
Ds0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Client Daten Initialisierung</name>
        <nameStyle>28,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ds0 f3 272 98 160 44 -72 -8 #rect
Ds0 f3 @|StepIcon #fIcon
Ds0 f4 actionDecl 'vzbank.in.test.Dyamex_Test_ProcessData out;
' #txt
Ds0 f4 actionTable 'out=in;
' #txt
Ds0 f4 actionCode 'import vzbank_integration_test.DyamexTest;
in.requestStr = DyamexTest.createJSON(in.client);' #txt
Ds0 f4 type vzbank.in.test.Dyamex_Test_ProcessData #txt
Ds0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Client Daten JSON Serialisierung</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ds0 f4 256 194 192 44 -91 -8 #rect
Ds0 f4 @|StepIcon #fIcon
Ds0 f5 actionDecl 'vzbank.in.test.Dyamex_Test_ProcessData out;
' #txt
Ds0 f5 actionTable 'out=in;
' #txt
Ds0 f5 actionCode 'import vzbank_integration_test.DyamexTest;
in.result = DyamexTest.CallDyamexPutClient(in.requestStr);' #txt
Ds0 f5 type vzbank.in.test.Dyamex_Test_ProcessData #txt
Ds0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Client Daten Service Aufruf</name>
        <nameStyle>27,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ds0 f5 272 290 160 44 -72 -8 #rect
Ds0 f5 @|StepIcon #fIcon
Ds0 f6 targetWindow NEW:card: #txt
Ds0 f6 targetDisplay TOP #txt
Ds0 f6 richDialogId vzbank.in.test.Ergebnisse #txt
Ds0 f6 startMethod start(String,String) #txt
Ds0 f6 type vzbank.in.test.Dyamex_Test_ProcessData #txt
Ds0 f6 requestActionDecl '<String result, String request> param;' #txt
Ds0 f6 requestMappingAction 'param.result=in.result;
param.request=in.requestStr;
' #txt
Ds0 f6 responseActionDecl 'vzbank.in.test.Dyamex_Test_ProcessData out;
' #txt
Ds0 f6 responseMappingAction 'out=in;
out.result=result.result;
' #txt
Ds0 f6 windowConfiguration '* ' #txt
Ds0 f6 isAsynch false #txt
Ds0 f6 isInnerRd false #txt
Ds0 f6 userContext '* ' #txt
Ds0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Ergebnisse</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ds0 f6 296 386 112 44 -32 -8 #rect
Ds0 f6 @|RichDialogIcon #fIcon
Ds0 f7 expr out #txt
Ds0 f7 111 64 352 98 #arcP
Ds0 f7 1 352 64 #addKink
Ds0 f7 0 0.7017457484980307 0 0 #arcLabel
Ds0 f2 expr out #txt
Ds0 f2 352 142 352 194 #arcP
Ds0 f8 expr out #txt
Ds0 f8 352 238 352 290 #arcP
Ds0 f9 expr out #txt
Ds0 f9 352 334 352 386 #arcP
Ds0 f10 expr out #txt
Ds0 f10 352 430 111 480 #arcP
Ds0 f10 1 352 480 #addKink
Ds0 f10 1 0.21736498742093102 0 0 #arcLabel
>Proto Ds0 .type vzbank.in.test.Dyamex_Test_ProcessData #txt
>Proto Ds0 .processKind NORMAL #txt
>Proto Ds0 0 0 32 24 18 0 #rect
>Proto Ds0 @|BIcon #fIcon
Ds0 f0 mainOut f7 tail #connect
Ds0 f7 head f3 mainIn #connect
Ds0 f3 mainOut f2 tail #connect
Ds0 f2 head f4 mainIn #connect
Ds0 f4 mainOut f8 tail #connect
Ds0 f8 head f5 mainIn #connect
Ds0 f5 mainOut f9 tail #connect
Ds0 f9 head f6 mainIn #connect
Ds0 f6 mainOut f10 tail #connect
Ds0 f10 head f1 mainIn #connect
