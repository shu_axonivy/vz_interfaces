package vzbank_integration_test;

import java.util.List;

public class DyamexClient
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -6493739965498196303L;

  private  String CustodianBankNumber;
  private  Boolean SingleClient;
  private  String WealthManagerLogin;
  private  String StartMandate;
  private  String BaseCurrencyIso3;
  private  String LanguageCode;
  private  String LastName;
  private  String FirstName;
  private  String Title;
  private  String Street;
  private  String Zip;
  private  String HomePlace;
  private  String CountryCodeIso2;
  private  String FinancialConsultantLogin;
  private  String CiresNumber;
  private  Boolean IncreasedRiskPep;
  private  String SignatureTypeCode;
  private  String OpeningDate;
  private  String WindowsUser;
  private  List<DyamexPerson> Persons;

  
  public String getCustodianBankNumber()
  {
    return CustodianBankNumber;
  }
  public void setCustodianBankNumber(String _CustodianBankNumber)
  {
    CustodianBankNumber = _CustodianBankNumber;
  }
  public Boolean getSingleClient()
  {
    return SingleClient;
  }
  public void setSingleClient(Boolean _SingleClient)
  {
    SingleClient = _SingleClient;
  }
  public String getWealthManagerLogin()
  {
    return WealthManagerLogin;
  }
  public void setWealthManagerLogin(String _WealthManagerLogin)
  {
    WealthManagerLogin = _WealthManagerLogin;
  }
  public String getStartMandate()
  {
    return StartMandate;
  }
  public void setStartMandate(String _StartMandate)
  {
    StartMandate = _StartMandate;
  }
  public String getBaseCurrencyIso3()
  {
    return BaseCurrencyIso3;
  }
  public void setBaseCurrencyIso3(String _BaseCurrencyIso3)
  {
    BaseCurrencyIso3 = _BaseCurrencyIso3;
  }
  public String getLanguageCode()
  {
    return LanguageCode;
  }
  public void setLanguageCode(String _LanguageCode)
  {
    LanguageCode = _LanguageCode;
  }
  public String getLastName()
  {
    return LastName;
  }
  public void setLastName(String _LastName)
  {
    LastName = _LastName;
  }
  public String getFirstName()
  {
    return FirstName;
  }
  public void setFirstName(String _FirstName)
  {
    FirstName = _FirstName;
  }
  public String getTitle()
  {
    return Title;
  }
  public void setTitle(String _Title)
  {
    Title = _Title;
  }
  public String getStreet()
  {
    return Street;
  }
  public void setStreet(String _Street)
  {
    Street = _Street;
  }
  public String getZip()
  {
    return Zip;
  }
  public void setZip(String _Zip)
  {
    Zip = _Zip;
  }
  public String getHomePlace()
  {
    return HomePlace;
  }
  public void setHomePlace(String _HomePlace)
  {
    HomePlace = _HomePlace;
  }
  public String getCountryCodeIso2()
  {
    return CountryCodeIso2;
  }
  public void setCountryCodeIso2(String _CountryCodeIso2)
  {
    CountryCodeIso2 = _CountryCodeIso2;
  }
  public String getFinancialConsultantLogin()
  {
    return FinancialConsultantLogin;
  }
  public void setFinancialConsultantLogin(String _FinancialConsultantLogin)
  {
    FinancialConsultantLogin = _FinancialConsultantLogin;
  }
  public String getCiresNumber()
  {
    return CiresNumber;
  }
  public void setCiresNumber(String _CiresNumber)
  {
    CiresNumber = _CiresNumber;
  }
  public Boolean getIncreasedRiskPep()
  {
    return IncreasedRiskPep;
  }
  public void setIncreasedRiskPep(Boolean _IncreasedRiskPep)
  {
    IncreasedRiskPep = _IncreasedRiskPep;
  }
  public String getSignatureTypeCode()
  {
    return SignatureTypeCode;
  }
  public void setSignatureTypeCode(String _SignatureTypeCode)
  {
    SignatureTypeCode = _SignatureTypeCode;
  }
  public String getOpeningDate()
  {
    return OpeningDate;
  }
  public void setOpeningDate(String _OpeningDate)
  {
    OpeningDate = _OpeningDate;
  }
  public String getWindowsUser()
  {
    return WindowsUser;
  }
  public void setWindowsUser(String _WindowsUser)
  {
    WindowsUser = _WindowsUser;
  }
  public List<DyamexPerson> getPersons()
  {
    return Persons;
  }
  public void setPersons(List<DyamexPerson> _Persons)
  {
    Persons = _Persons;
  }
}
