package vzbank_integration_test;


public class DyamexContactType
{

  private static final long serialVersionUID = 2924147486178969700L;
  private String ContactTypeCode;
  private String ContactValue;

  
  

  public java.lang.String getContactValue()
  {
    return ContactValue;
  }

  public void setContactValue(java.lang.String _ContactValue)
  {
    ContactValue = _ContactValue;
  }

  public java.lang.String getContactTypeCode()
  {
    return ContactTypeCode;
  }

  public void setContactTypeCode(java.lang.String _ContactTypeCode)
  {
    ContactTypeCode = _ContactTypeCode;
  }

}
