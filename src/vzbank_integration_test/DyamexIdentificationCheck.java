package vzbank_integration_test;

public class DyamexIdentificationCheck
{
  private static final long serialVersionUID = 3339406849358637535L;

  private String DocumentTypeCode;
  private String PlaceOfIssue;
  private String DocumentId;
  private String CheckTypeCode;
  private String IssueDate;
  private String CountryOfIssueIso2;
  private String ExpiryDate;

  
  public java.lang.String getDocumentTypeCode()
  {
    return DocumentTypeCode;
  }

  public void setDocumentTypeCode(java.lang.String _DocumentTypeCode)
  {
    DocumentTypeCode = _DocumentTypeCode;
  }


  public java.lang.String getDocumentId()
  {
    return DocumentId;
  }

  public void setDocumentId(java.lang.String _DocumentId)
  {
    DocumentId = _DocumentId;
  }


  public java.lang.String getCheckTypeCode()
  {
    return CheckTypeCode;
  }

  public void setCheckTypeCode(java.lang.String _CheckTypeCode)
  {
    CheckTypeCode = _CheckTypeCode;
  }


  public java.lang.String getIssueDate()
  {
    return IssueDate;
  }

  public void setIssueDate(java.lang.String _IssueDate)
  {
    IssueDate = _IssueDate;
  }


  public java.lang.String getExpiryDate()
  {
    return ExpiryDate;
  }

  public void setExpiryDate(java.lang.String _ExpiryDate)
  {
    ExpiryDate = _ExpiryDate;
  }


  public java.lang.String getCountryOfIssueIso2()
  {
    return CountryOfIssueIso2;
  }

  public void setCountryOfIssueIso2(java.lang.String _CountryOfIssueIso2)
  {
    CountryOfIssueIso2 = _CountryOfIssueIso2;
  }


  public java.lang.String getPlaceOfIssue()
  {
    return PlaceOfIssue;
  }

  public void setPlaceOfIssue(java.lang.String _PlaceOfIssue)
  {
    PlaceOfIssue = _PlaceOfIssue;
  }

}
