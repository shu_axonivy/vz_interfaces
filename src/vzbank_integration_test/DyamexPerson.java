package vzbank_integration_test;

import java.util.List;

public class DyamexPerson extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 718235284916301975L;

  private String CustodianBankNumber;
  private String FirstName;
  private String LastName;
  private String GenderCode;
  private String Title;
  private String Street;
  private String Zip;
  private String BirthDay;
  private String HomePlace;
  private String RelationTypeCode;
  private String CountryCodeIso2;
  private List<DyamexContactType> ContactTypes;
  private String NationalityCodeIso2;
  private DyamexIdentificationCheck IdentificationCheck;
  private String LanguageCode;

  
  
  
  
  public String getCustodianBankNumber()
  {
    return CustodianBankNumber;
  }

  
  public void setCustodianBankNumber(String _CustodianBankNumber)
  {
    CustodianBankNumber = _CustodianBankNumber;
  }

  
 
  public String getFirstName()
  {
    return FirstName;
  }

  
  public void setFirstName(String _FirstName)
  {
    FirstName = _FirstName;
  }


  
  public String getLastName()
  {
    return LastName;
  }

  
  public void setLastName(String _LastName)
  {
    LastName = _LastName;
  }


  public String getGenderCode()
  {
    return GenderCode;
  }

  public void setGenderCode(String _GenderCode)
  {
    GenderCode = _GenderCode;
  }


  public String getTitle()
  {
    return Title;
  }

  public void setTitle(String _Title)
  {
    Title = _Title;
  }


  public String getStreet()
  {
    return Street;
  }

  public void setStreet(String _Street)
  {
    Street = _Street;
  }



  public String getZip()
  {
    return Zip;
  }


  public void setZip(String _Zip)
  {
    Zip = _Zip;
  }



  public String getHomePlace()
  {
    return HomePlace;
  }

  public void setHomePlace(String _HomePlace)
  {
    HomePlace = _HomePlace;
  }


  public String getBirthDay()
  {
    return BirthDay;
  }

  public void setBirthDay(String _BirthDay)
  {
    BirthDay = _BirthDay;
  }


  public String getCountryCodeIso2()
  {
    return CountryCodeIso2;
  }

  public void setCountryCodeIso2(String _CountryCodeIso2)
  {
    CountryCodeIso2 = _CountryCodeIso2;
  }


  public String getNationalityCodeIso2()
  {
    return NationalityCodeIso2;
  }

  public void setNationalityCodeIso2(String _NationalityCodeIso2)
  {
    NationalityCodeIso2 = _NationalityCodeIso2;
  }


  public String getLanguageCode()
  {
    return LanguageCode;
  }

  public void setLanguageCode(String _LanguageCode)
  {
    LanguageCode = _LanguageCode;
  }


  public DyamexIdentificationCheck getIdentificationCheck()
  {
    return IdentificationCheck;
  }

  public void setIdentificationCheck(DyamexIdentificationCheck _IdentificationCheck)
  {
    IdentificationCheck = _IdentificationCheck;
  }


  public List<DyamexContactType> getContactTypes()
  {
    return ContactTypes;
  }

  public void setContactTypes(List<DyamexContactType> _ContactTypes)
  {
    ContactTypes = _ContactTypes;
  }


  public String getRelationTypeCode()
  {
    return RelationTypeCode;
  }

  public void setRelationTypeCode(String _RelationTypeCode)
  {
    RelationTypeCode = _RelationTypeCode;
  }

}
