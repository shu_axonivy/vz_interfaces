package vzbank_integration_test;

import java.util.ArrayList;
import java.util.List;





import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ch.ivyteam.ivy.environment.Ivy;

import com.google.gson.*;

public class DyamexTest {

	public static DyamexClient initDyamexClient(){
		DyamexClient dyamexClient = new DyamexClient(); 
		
		
		
		dyamexClient.setCustodianBankNumber("688.000.07");
		dyamexClient.setSingleClient(false);
		dyamexClient.setWealthManagerLogin("AAk");

		dyamexClient.setStartMandate("2017-08-01");
		//"BaseCurrencyIso3": "CHF",
		dyamexClient.setBaseCurrencyIso3("CHF");
		//"LanguageCode": "de-CH",
		dyamexClient.setLanguageCode("de-CH");
		//"LastName": "M�ller-Bossard Elisabeth und",
		dyamexClient.setLastName("M�ller-Bossard Elisabeth und");
		//"FirstName": "Moser Katharina",
		dyamexClient.setFirstName("Moser Katharina");
		//"Title": "",
		dyamexClient.setTitle("Professor");
		//"Street": "Weidenrain 8",
		dyamexClient.setStreet("Weidenrain 8");
		//"Zip": 3084,
		dyamexClient.setZip("3084");
		//"HomePlace": "Wabern",
		dyamexClient.setHomePlace("Wabern");
		//"CountryCodeIso2": "CH",
		dyamexClient.setCountryCodeIso2("CH");
		//"FinancialConsultantLogin": "AAk",
		dyamexClient.setFinancialConsultantLogin("AAk");
		//"CiresNumber": 9999913,
		dyamexClient.setCiresNumber("9999913");
		//"IncreasedRiskPep": false,
		dyamexClient.setIncreasedRiskPep(false);
		//"SignatureTypeCode": 2,
		dyamexClient.setSignatureTypeCode("2");
		//"OpeningDate": "2017-08-01 11:17:00.001",
		dyamexClient.setOpeningDate("2017-08-01 11:17:00.001");
		//"WindowsUser": "JJec",
		dyamexClient.setWindowsUser("JJec");
		
		List<DyamexPerson> listPersons = new ArrayList<DyamexPerson>(); 
			
		
		DyamexIdentificationCheck idc = new DyamexIdentificationCheck();
		
		// "DocumentTypeCode": "ORIGINAL_ID",
		idc.setDocumentTypeCode("ORIGINAL_ID");		
		// "DocumentId": "",
		idc.setCheckTypeCode("");		
		//"CheckTypeCode": "PCCPICTDPV",
		idc.setCheckTypeCode("PCCPICTDPV");
		//"IssueDate": "",
		idc.setIssueDate("");		
		//"ExpiryDate": "",
		idc.setExpiryDate("");		
		//"CountryOfIssueIso2": "",
		idc.setCountryOfIssueIso2("");
		//"PlaceOfIssue": "",
		idc.setPlaceOfIssue("");
		
		DyamexContactType ct = new DyamexContactType(); 
		//"ContactValue": "079 333 22 11",
		ct.setContactValue("079 333 22 11");
		//"ContactTypeCode": "MOBILE_PHONE" 
		ct.setContactTypeCode("MOBILE_PHONE");
		
		List<DyamexContactType> listCT = new ArrayList<DyamexContactType>();
		listCT.add(ct);
		
		DyamexPerson person1 = new DyamexPerson();
		
		//"CustodianBankNumber": "544.518.85",
		person1.setCustodianBankNumber("544.518.85");
    	//"FirstName": "Elisabeth", 
		person1.setFirstName("Elisabeth");
    	//"LastName": "M�ller-Bossard",
		person1.setLastName("M�ller-Bossard");
    	//"GenderCode": "female",
		person1.setGenderCode("female");
    	//"Title": " ",
		person1.setTitle("Professor");
    	//"Street": "Zugerbergstrasse 38",
		person1.setStreet("Zugerbergstrasse 38");
    	//"Zip": 6300,
		person1.setZip("6300");
    	//"HomePlace": "Zug",
		person1.setHomePlace("Zug");
    	//"BirthDay": "1946-05-29",
		person1.setBirthDay("1946-05-29");
    	//"CountryCodeIso2": "CH",
		person1.setCountryCodeIso2("CH");
    	//"NationalityCodeIso2": "CH",
		person1.setNationalityCodeIso2("CH");
    	//"LanguageCode": "de-CH",
		person1.setLanguageCode("de-CH");
		
		person1.setIdentificationCheck(idc);
		person1.setContactTypes(listCT);
		
		listPersons.add(person1);
		
		
		DyamexPerson person2 = new DyamexPerson();
		
		//"CustodianBankNumber": "544.518.85",
		person2.setCustodianBankNumber("544.518.85");
    	//"FirstName": "Elisabeth", 
		person2.setFirstName("Elisabeth");
    	//"LastName": "M�ller-Bossard",
		person2.setLastName("M�ller-Bossard");
    	//"GenderCode": "female",
		person2.setGenderCode("female");
    	//"Title": " ",
		person2.setTitle("Professor");
    	//"Street": "Zugerbergstrasse 38",
		person2.setStreet("Zugerbergstrasse 38");
    	//"Zip": 6300,
		person2.setZip("6300");
    	//"HomePlace": "Zug",
		person2.setHomePlace("Zug");
    	//"BirthDay": "1946-05-29",
		person2.setBirthDay("1946-05-29");
    	//"CountryCodeIso2": "CH",
		person2.setCountryCodeIso2("CH");
    	//"NationalityCodeIso2": "CH",
		person2.setNationalityCodeIso2("CH");
    	//"LanguageCode": "de-CH",
		person2.setLanguageCode("de-CH");
		
		person2.setIdentificationCheck(idc);
		person2.setContactTypes(listCT);
		
		listPersons.add(person2);
		dyamexClient.setPersons(listPersons);
		return dyamexClient;
	}
	
	
	
	public static String createJSON(DyamexClient dyamexClient){
		Gson gson = new Gson();
		String jsonStr =  gson.toJson(dyamexClient);
		return jsonStr;
	}
	
	
	public static String CallDyamexPutClient(String request)
	{
		Entity<String> entity = Entity.entity(request, MediaType.APPLICATION_JSON);
		Response getInfo = Ivy.rest().client("DyamexPutClient").request().put(entity);
		return getInfo.toString();
	}
}
