package vzbank.in.test;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Dyamex_Test_ProcessData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Dyamex_Test_ProcessData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -3967391632202844788L;

  private transient java.lang.String result;

  /**
   * Gets the field result.
   * @return the value of the field result; may be null.
   */
  public java.lang.String getResult()
  {
    return result;
  }

  /**
   * Sets the field result.
   * @param _result the new value of the field result.
   */
  public void setResult(java.lang.String _result)
  {
    result = _result;
  }

  private transient java.lang.String requestStr;

  /**
   * Gets the field requestStr.
   * @return the value of the field requestStr; may be null.
   */
  public java.lang.String getRequestStr()
  {
    return requestStr;
  }

  /**
   * Sets the field requestStr.
   * @param _requestStr the new value of the field requestStr.
   */
  public void setRequestStr(java.lang.String _requestStr)
  {
    requestStr = _requestStr;
  }

  private transient vzbank_integration_test.DyamexClient client;

  /**
   * Gets the field client.
   * @return the value of the field client; may be null.
   */
  public vzbank_integration_test.DyamexClient getClient()
  {
    return client;
  }

  /**
   * Sets the field client.
   * @param _client the new value of the field client.
   */
  public void setClient(vzbank_integration_test.DyamexClient _client)
  {
    client = _client;
  }

}
