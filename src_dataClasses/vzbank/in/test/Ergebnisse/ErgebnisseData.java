package vzbank.in.test.Ergebnisse;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ErgebnisseData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ErgebnisseData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -2924147472631597189L;

  private java.lang.String result;

  /**
   * Gets the field result.
   * @return the value of the field result; may be null.
   */
  public java.lang.String getResult()
  {
    return result;
  }

  /**
   * Sets the field result.
   * @param _result the new value of the field result.
   */
  public void setResult(java.lang.String _result)
  {
    result = _result;
  }

  private java.lang.String requestStr;

  /**
   * Gets the field requestStr.
   * @return the value of the field requestStr; may be null.
   */
  public java.lang.String getRequestStr()
  {
    return requestStr;
  }

  /**
   * Sets the field requestStr.
   * @param _requestStr the new value of the field requestStr.
   */
  public void setRequestStr(java.lang.String _requestStr)
  {
    requestStr = _requestStr;
  }

}
